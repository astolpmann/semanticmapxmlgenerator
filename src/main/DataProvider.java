/*
 * Copyright (c) 2012, Andreas Stolpmann <andisto@informatik.uni-bremen.de>
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the Intelligent Autonomous Systems Group/
 *       Universitaet Bremen nor the names of its contributors 
 *       may be used to endorse or promote products derived from this software 
 *       without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
package main;

import io.CodeStream;
import io.FileIO;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.atomic.AtomicBoolean;

import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.swing.filechooser.FileFilter;

import data.SMGProject;
import data.SemanticMapObject;

public class DataProvider {

    private int smoCounter = 0;

    protected AtomicBoolean dataHasChanged;

    private SMGProject currentProject;
    private int currentSMO;

    public static final SemanticMapObject dummySMO = new SemanticMapObject();

    public DataProvider() {
        currentProject = new SMGProject();
        dataHasChanged = new AtomicBoolean(false);
    }

    public List<SemanticMapObject> getCurrentSMOList() {
        return currentProject.smoList;
    }

    public void setCurrentSMO(int index) {
        SMG.debugOutput("DataProvider: currentSMO is now: " + index);
        currentSMO = index;
    }

    public SemanticMapObject getCurrentSMO() {
        return currentSMO < 0 || currentSMO >= currentProject.smoList.size()
                ? null
                : currentProject.smoList.get(currentSMO);
    }
    
    public int getCurrentSMOIndex() {
        return currentSMO;
    }

    public boolean renameSMO(String name) {

        for (SemanticMapObject smo : currentProject.smoList) {
            if (smo.name.equalsIgnoreCase(name) && !smo.equals(getCurrentSMO()))
                return false;
        }

        getCurrentSMO().name = name;         
        SemanticMapObject temp = getCurrentSMO();        
        Collections.sort(currentProject.smoList);        
        int newSmoIndex = currentProject.smoList.indexOf(temp);        
        SMG.debugOutput("DataProvider: renameSMO - newSmoIndex = " + newSmoIndex);        
        assert(newSmoIndex >= 0 && newSmoIndex < currentProject.smoList.size());
        setCurrentSMO(newSmoIndex);
        
        return true;
    }

    public void createNewSMO() {
        SemanticMapObject newSMO = new SemanticMapObject();
        newSMO.name = "< new object " + smoCounter++ + " >";
        currentProject.smoList.add(newSMO);

        dataHasChanged.set(true);        
        Collections.sort(currentProject.smoList);
        
        int newSmoIndex = currentProject.smoList.indexOf(newSMO);
        assert(newSmoIndex >= 0 && newSmoIndex < currentProject.smoList.size());
        setCurrentSMO(newSmoIndex);
        
        SMG.getGUI().addedNewSMO();
    }

    public void deleteCurrentSMO() {
        if (currentSMO == -1 || currentProject.smoList.isEmpty())
            return;

        if (SMG.skipDataLossWarnings
                || JOptionPane.showConfirmDialog(SMG.getGUI(),
                        "Are you sure you want to delete this object?",
                        "Delete Object", JOptionPane.YES_NO_OPTION,
                        JOptionPane.INFORMATION_MESSAGE) == JOptionPane.YES_OPTION) {

            currentProject.smoList.remove(currentSMO);
            currentSMO = -1;

            dataHasChanged.set(true);

            SMG.getGUI().removeCurrentSMO();
        }
    }

    public void newProject() {
        if (!SMG.skipDataLossWarnings && dataHasChanged.get()
                && JOptionPane.showConfirmDialog(SMG.getGUI(),
                        "Any unsaved changes will be lost. Continue?",
                        "Create new Project", JOptionPane.YES_NO_OPTION,
                        JOptionPane.WARNING_MESSAGE) == JOptionPane.NO_OPTION) {
            return;
        }
            
            this.currentProject = new SMGProject();
            dataHasChanged.set(false);
            smoCounter = 0;

            SMG.getGUI().updateFull();
    }

    public void save() {
        if(currentProject.path == null) {
            saveAs();
            return;
        }
        
        SMG.debugOutput("saving...");
        
        FileIO io = new FileIO();
        try {
            io.saveProject(currentProject, new File(currentProject.path));
            dataHasChanged.set(false);
        } catch (IOException e) {
            SMG.debugOutput(e);
            JOptionPane.showMessageDialog(SMG.getGUI(),
                    "There was an error saving to that file!", "Error",
                    JOptionPane.ERROR_MESSAGE);
        }
    }
    

    public void saveAs() {
        SMG.debugOutput("saving as...");
        
        JFileChooser fc = new JFileChooser();
        fc.setFileFilter(new SMOFilter());
        fc.setAcceptAllFileFilterUsed(false);

        if (fc.showSaveDialog(SMG.getGUI()) == JFileChooser.APPROVE_OPTION) {
            File file = fc.getSelectedFile();
            if (!file.getName().toLowerCase().endsWith(".smg"))
                file = new File(fc.getSelectedFile() + ".smg");

            if (file.exists()) {
                int result = JOptionPane.showConfirmDialog(SMG.getGUI(),
                        file.getAbsolutePath() + " already exists. Overwrite?",
                        "Overwrite file", JOptionPane.YES_NO_OPTION,
                        JOptionPane.QUESTION_MESSAGE);
                if (result == JOptionPane.NO_OPTION) {
                    return;
                }
            }

            FileIO io = new FileIO();
            try {
                currentProject.path = file.getAbsolutePath();
                SMG.debugOutput("current projects location is now: " + currentProject.path);
                io.saveProject(currentProject, file);
                dataHasChanged.set(false);
            } catch (IOException e) {
                SMG.debugOutput(e);
                JOptionPane.showMessageDialog(SMG.getGUI(),
                        "There was an error saving to that file!", "Error",
                        JOptionPane.ERROR_MESSAGE);
            }
        }
    }

    public void load() {
        if(!SMG.skipDataLossWarnings && dataHasChanged.get()) {        
            int result = JOptionPane.showConfirmDialog(SMG.getGUI(),
                    "Any unsafed work will be lost!", "Warning",
                    JOptionPane.OK_CANCEL_OPTION, JOptionPane.WARNING_MESSAGE);
            if (result == JOptionPane.CANCEL_OPTION) {
                return;
            }
        }

        JFileChooser fc = new JFileChooser();
        fc.setFileFilter(new SMOFilter());
        fc.setAcceptAllFileFilterUsed(false);

        if (fc.showOpenDialog(SMG.getGUI()) == JFileChooser.APPROVE_OPTION) {
            File file = fc.getSelectedFile();

            if (file.exists() && file.canRead()
                    && file.getName().toLowerCase().endsWith(".smg")) {
                FileIO io = new FileIO();
                try {
                    currentProject = io.loadProject(file);
                    currentProject.path = file.getAbsolutePath();
                    dataHasChanged.set(false);
                } catch (ClassNotFoundException e) {
                    SMG.debugOutput(e);
                    JOptionPane.showMessageDialog(SMG.getGUI(),
                            "Could not load file: File corrupted", "Error",
                            JOptionPane.ERROR_MESSAGE);
                    currentProject = new SMGProject();
                } catch (IOException e) {
                    SMG.debugOutput(e);
                    JOptionPane.showMessageDialog(SMG.getGUI(),
                            "There was an error reading the file!", "Error",
                            JOptionPane.ERROR_MESSAGE);
                    currentProject = new SMGProject();
                }

                smoCounter = currentProject.smoList.size();
                SemanticMapObject.nextID = currentProject.smoList.size() + 1;
            } else {
                JOptionPane.showMessageDialog(SMG.getGUI(),
                        "Please choose a valid '.smoc' file!", "Error",
                        JOptionPane.ERROR_MESSAGE);
            }
        }

        SMG.getGUI().updateFull();
        SMG.getGUI().updateCurrentSMO();
    }

    public void export() {
        if (SMG.exportToConsole) {
            CodeStream out = new CodeStream(4);
            currentProject.generateCode(out);
            return;
        }

        JFileChooser fc = new JFileChooser();
        fc.setFileFilter(new OWLFilter());
        fc.setAcceptAllFileFilterUsed(false);

        if (fc.showDialog(SMG.getGUI(), "Export") == JFileChooser.APPROVE_OPTION) {
            File file = fc.getSelectedFile();
            if (!file.getName().toLowerCase().endsWith(".owl"))
                file = new File(fc.getSelectedFile() + ".owl");

            if (file.exists()) {
                int result = JOptionPane.showConfirmDialog(SMG.getGUI(),
                        file.getAbsolutePath() + " already exists. Overwrite?",
                        "Overwrite file", JOptionPane.YES_NO_OPTION,
                        JOptionPane.QUESTION_MESSAGE);
                if (result == JOptionPane.NO_OPTION) {
                    return;
                }
            }

            CodeStream out;
            try {
                out = new CodeStream(file, 4);
                currentProject.generateCode(out);
            } catch (FileNotFoundException e) {
                SMG.debugOutput(e);
                JOptionPane.showMessageDialog(SMG.getGUI(),
                        "There was an error exporting to that file!", "Error",
                        JOptionPane.ERROR_MESSAGE);;
            }
        }
    }

    private class SMOFilter extends FileFilter {
        @Override
        public boolean accept(File file) {
            return file.isDirectory()
                    || file.getName().toLowerCase().endsWith(".smg");
        }

        @Override
        public String getDescription() {
            return "smg project files (*.smg)";
        }
    }

    private class OWLFilter extends FileFilter {
        @Override
        public boolean accept(File file) {
            return file.isDirectory()
                    || file.getName().toLowerCase().endsWith(".owl");
        }

        @Override
        public String getDescription() {
            return "owl files (*.owl)";
        }
    }
}