/*
 * Copyright (c) 2012, Andreas Stolpmann <andisto@informatik.uni-bremen.de>
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the Intelligent Autonomous Systems Group/
 *       Universitaet Bremen nor the names of its contributors 
 *       may be used to endorse or promote products derived from this software 
 *       without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
package main;

import gui.SMGGUI;

import java.io.PrintStream;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.swing.JOptionPane;
import javax.swing.UIManager;

public final class SMG {
    public static final String NAME = "SemanticMapObjectXMLCreator";
    public static final String VERSION = "0.2";

    private static SMG theInstance = null;

    public static boolean debugMode = false;
    public static boolean exportToConsole = false;
    public static boolean skipDataLossWarnings = false;

    private SMGGUI gui;
    private DataProvider dataProvider;

    private SMG() {
        this.gui = new SMGGUI();
        this.dataProvider = new DataProvider();
    }

    public synchronized static SMG getSMG() {
        if (theInstance == null)
            theInstance = new SMG();
        return theInstance;
    }
    
    public static DataProvider getDataProvider() {
        return SMG.getSMG().dataProvider;
    }
    
    public static SMGGUI getGUI() {
        return SMG.getSMG().gui;
    }

    public void close() {
        if (skipDataLossWarnings)
            System.exit(0);

        if (!dataProvider.dataHasChanged.get()
                || JOptionPane.showConfirmDialog(SMG.getSMG().gui,
                        "Any unsaved changes will be lost. Exit now?", "Exit",
                        JOptionPane.YES_NO_OPTION, JOptionPane.WARNING_MESSAGE) == JOptionPane.YES_OPTION) {
            System.exit(0);
        }
    }

    public static void debugOutput(Throwable e) {
        if (!debugMode)
            return;

        e.printStackTrace();
    }

    public static void debugOutput(String s) {
        debugOutput(s, false);
    }

    public static void debugOutput(String s, boolean useSystemErr) {
        if (!debugMode)
            return;
        
        Date date = new Date();
        String time = new SimpleDateFormat("dd.MM.yyyy HH:mm:ss.SSS").format(date);

        if (useSystemErr)
            System.err.println(time + ": " + s);
        else
            System.out.println(time + ": " + s);
    }

    public static void printHelp(PrintStream out) {
        out.println("Available options:");
        out.println("    -c               Export to console instead of a file");
        out.println("    -d    --debug    Show additional debug information");
        out.println("    -h    --help     Show this help");
        out.println("    -w    --nowarn   Don't show warning messages when data will be lost (debug)");
    }

    public static void main(String[] args) {

        for (int i = 0; i < args.length; ++i) {
            if (args[i].equalsIgnoreCase("-C")) {
                exportToConsole = true;
            } else if (args[i].equalsIgnoreCase("-D")
                    || args[i].equalsIgnoreCase("--DEBUG")) {
                debugMode = true;
            } else if (args[i].equalsIgnoreCase("-H")
                    || args[i].equalsIgnoreCase("--HELP")) {
                printHelp(System.out);
            } else if (args[i].equalsIgnoreCase("-W")
                    || args[i].equalsIgnoreCase("--NOWARN")) {
                skipDataLossWarnings = true;
            } else {
                System.err.println("Unknown option: " + args[i]);
                printHelp(System.err);
                return;
            }
        }

        try {
            UIManager.setLookAndFeel(UIManager
                    .getCrossPlatformLookAndFeelClassName());
            // UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
        } catch (Exception e) {
            SMG.debugOutput(e);
        }
        getSMG();
    }
}
