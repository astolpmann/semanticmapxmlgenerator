/*
 * Copyright (c) 2012, Andreas Stolpmann <andisto@informatik.uni-bremen.de>
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the Intelligent Autonomous Systems Group/
 *       Universitaet Bremen nor the names of its contributors 
 *       may be used to endorse or promote products derived from this software 
 *       without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

package gui.dataPanel;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;

import javax.swing.BorderFactory;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.SwingConstants;

import data.SemanticMapObject;

import main.DataProvider;
import main.SMG;

public class TranslationPanel extends JPanel {
    private static final long serialVersionUID = 8317777452043571988L;

    private final JLabel xLabel = new JLabel("x:", SwingConstants.TRAILING);
    private final JLabel yLabel = new JLabel("y:", SwingConstants.TRAILING);
    private final JLabel zLabel = new JLabel("z:", SwingConstants.TRAILING);

    private final JTextField xTextField = new JTextField();
    private final JTextField yTextField = new JTextField();
    private final JTextField zTextField = new JTextField();

    public TranslationPanel() {
        this.setBorder(BorderFactory.createCompoundBorder(
                BorderFactory.createTitledBorder("Translation"),
                BorderFactory.createEmptyBorder(5, 5, 5, 5)));

        this.setLayout(new GridBagLayout());
        GridBagConstraints tpc = new GridBagConstraints();
        tpc.fill = GridBagConstraints.HORIZONTAL;
        tpc.anchor = GridBagConstraints.PAGE_START;

        tpc.gridx = 0;
        tpc.insets = new Insets(3, 3, 3, 3);
        tpc.weightx = 0;
        this.add(xLabel, tpc);

        tpc.gridx = 1;
        tpc.insets = new Insets(3, 3, 3, 6);
        tpc.weightx = 1;
        this.add(xTextField, tpc);

        tpc.gridx = 2;
        tpc.insets = new Insets(3, 3, 3, 3);
        tpc.weightx = 0;
        this.add(yLabel, tpc);

        tpc.gridx = 3;
        tpc.insets = new Insets(3, 3, 3, 6);
        tpc.weightx = 1;
        this.add(yTextField, tpc);

        tpc.gridx = 4;
        tpc.insets = new Insets(3, 3, 3, 3);
        tpc.weightx = 0;
        this.add(zLabel, tpc);

        tpc.gridx = 5;
        tpc.weightx = 1;
        this.add(zTextField, tpc);

        xTextField.addActionListener(saveActionListener());
        xTextField.addFocusListener(saveFocusListener());
        yTextField.addActionListener(saveActionListener());
        yTextField.addFocusListener(saveFocusListener());
        zTextField.addActionListener(saveActionListener());
        zTextField.addFocusListener(saveFocusListener());

        setEnabledTranslationPanel(false);
    }

    private void setEnabledTranslationPanel(boolean enabled) {
        xTextField.setEnabled(enabled);
        xLabel.setEnabled(enabled);
        yTextField.setEnabled(enabled);
        yLabel.setEnabled(enabled);
        zTextField.setEnabled(enabled);
        zLabel.setEnabled(enabled);
    }

    private ActionListener saveActionListener() {
        return new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent arg0) {
                SMG.debugOutput("TranslationPanel: saveActionListener - actionPerformed");
                saveData();
            }
        };
    }

    private FocusListener saveFocusListener() {
        return new FocusAdapter() {
            @Override
            public void focusLost(FocusEvent e) {
                SMG.debugOutput("TranslationPanel: saveFocusListener - focusLost");
                saveData();
            }
            @Override
            public void focusGained(FocusEvent e) {
                if(e.getComponent() instanceof JTextField) {
                    JTextField t = (JTextField)(e.getComponent());
                    t.setSelectionStart(0);
                    t.setSelectionEnd(t.getText().length());
                }
            }
        };
    }

    public void updateFull() {
        SMG.debugOutput("TranslationPanel: updateFull");

        SemanticMapObject smo = SMG.getDataProvider().getCurrentSMO();
        if (smo == null) {
            smo = DataProvider.dummySMO;
            this.setEnabledTranslationPanel(false);
        } else {
            this.setEnabledTranslationPanel(true);
        }

        xTextField.setText(Double.toString(smo.translation.x));
        yTextField.setText(Double.toString(smo.translation.y));
        zTextField.setText(Double.toString(smo.translation.z));
    }

    private void saveData() {
        SMG.debugOutput("TranslationPanel: saveData");
        try {
            double newX = Double.parseDouble(xTextField.getText());
            double newY = Double.parseDouble(yTextField.getText());
            double newZ = Double.parseDouble(zTextField.getText());

            SMG.getDataProvider().getCurrentSMO().translation.x = newX;
            SMG.getDataProvider().getCurrentSMO().translation.y = newY;
            SMG.getDataProvider().getCurrentSMO().translation.z = newZ;
        } catch (NumberFormatException e) {
            SMG.debugOutput("TranslationPanel: saveData - NumberFormatException, reseting translation");
            updateFull();
        }
    }
}
