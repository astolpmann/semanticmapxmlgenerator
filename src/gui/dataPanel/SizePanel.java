/*
 * Copyright (c) 2012, Andreas Stolpmann <andisto@informatik.uni-bremen.de>
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the Intelligent Autonomous Systems Group/
 *       Universitaet Bremen nor the names of its contributors 
 *       may be used to endorse or promote products derived from this software 
 *       without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

package gui.dataPanel;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;

import javax.swing.BorderFactory;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.SwingConstants;

import data.SemanticMapObject;

import main.DataProvider;
import main.SMG;

public class SizePanel extends JPanel {
    private static final long serialVersionUID = -1696274691121729938L;

    private final JLabel wLabel = new JLabel("Width:", SwingConstants.TRAILING);
    private final JLabel hLabel = new JLabel("Height:", SwingConstants.TRAILING);
    private final JLabel dLabel = new JLabel("Depth:", SwingConstants.TRAILING);

    private final JTextField wTextField = new JTextField();
    private final JTextField hTextField = new JTextField();
    private final JTextField dTextField = new JTextField();

    public SizePanel() {
        this.setBorder(BorderFactory.createCompoundBorder(
                BorderFactory.createTitledBorder("Size"),
                BorderFactory.createEmptyBorder(5, 5, 5, 5)));

        this.setLayout(new GridBagLayout());
        GridBagConstraints spc = new GridBagConstraints();
        spc.fill = GridBagConstraints.HORIZONTAL;
        spc.anchor = GridBagConstraints.PAGE_START;

        spc.gridx = 0;
        spc.weightx = 0;
        spc.insets = new Insets(3, 3, 3, 3);
        this.add(wLabel, spc);

        spc.gridx = 1;
        spc.weightx = 1;
        spc.insets = new Insets(3, 3, 3, 6);
        this.add(wTextField, spc);

        spc.gridx = 2;
        spc.weightx = 0;
        spc.insets = new Insets(3, 3, 3, 3);
        this.add(hLabel, spc);

        spc.gridx = 3;
        spc.weightx = 1;
        spc.insets = new Insets(3, 3, 3, 6);
        this.add(hTextField, spc);

        spc.gridx = 4;
        spc.weightx = 0;
        spc.insets = new Insets(3, 3, 3, 3);
        this.add(dLabel, spc);

        spc.gridx = 5;
        spc.weightx = 1;
        this.add(dTextField, spc);

        wTextField.addActionListener(saveActionListener());
        wTextField.addFocusListener(saveFocusListener());
        hTextField.addActionListener(saveActionListener());
        hTextField.addFocusListener(saveFocusListener());
        dTextField.addActionListener(saveActionListener());
        dTextField.addFocusListener(saveFocusListener());

        this.setEnabledSizePanel(false);
    }

    private void setEnabledSizePanel(boolean enabled) {
        wTextField.setEnabled(enabled);
        wLabel.setEnabled(enabled);
        hTextField.setEnabled(enabled);
        hLabel.setEnabled(enabled);
        dTextField.setEnabled(enabled);
        dLabel.setEnabled(enabled);
    }

    private ActionListener saveActionListener() {
        return new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent arg0) {
                SMG.debugOutput("SizePanel: saveActionListener - actionPerformed");
                saveData();
            }
        };
    }

    private FocusListener saveFocusListener() {
        return new FocusAdapter() {
            @Override
            public void focusLost(FocusEvent e) {
                SMG.debugOutput("SizePanel: saveFocusListener - focusLost");
                saveData();
            }
            @Override
            public void focusGained(FocusEvent e) {
                if(e.getComponent() instanceof JTextField) {
                    JTextField t = (JTextField)(e.getComponent());
                    t.setSelectionStart(0);
                    t.setSelectionEnd(t.getText().length());
                }
            }
        };
    }

    public void updateFull() {
        SMG.debugOutput("SizePanel: updateFull");

        SemanticMapObject smo = SMG.getDataProvider().getCurrentSMO();
        if (smo == null) {
            smo = DataProvider.dummySMO;
            this.setEnabledSizePanel(false);
        } else {
            this.setEnabledSizePanel(true);
        }

        wTextField.setText(Double.toString(smo.size.x));
        hTextField.setText(Double.toString(smo.size.y));
        dTextField.setText(Double.toString(smo.size.z));
    }

    public void saveData() {
        SMG.debugOutput("SizePanel: saveData");
        
        try {
            double newX = Double.parseDouble(wTextField.getText());
            double newY = Double.parseDouble(hTextField.getText());
            double newZ = Double.parseDouble(dTextField.getText());

            SMG.getDataProvider().getCurrentSMO().size.x = newX;
            SMG.getDataProvider().getCurrentSMO().size.y = newY;
            SMG.getDataProvider().getCurrentSMO().size.z = newZ;
        } catch (NumberFormatException e) {
            SMG.debugOutput("SizePanel: saveData - NumberFormatException, reseting size");
            updateFull();
        }
    }
}
