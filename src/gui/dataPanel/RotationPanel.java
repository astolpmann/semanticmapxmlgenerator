/*
 * Copyright (c) 2012, Andreas Stolpmann <andisto@informatik.uni-bremen.de>
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the Intelligent Autonomous Systems Group/
 *       Universitaet Bremen nor the names of its contributors 
 *       may be used to endorse or promote products derived from this software 
 *       without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

package gui.dataPanel;

import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;

import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.ButtonGroup;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JTextField;
import javax.swing.SwingConstants;

import main.DataProvider;
import main.SMG;
import math.CommonMath;
import math.Matrix3x3;
import math.Vector3;
import data.SemanticMapObject;

public class RotationPanel extends JPanel {
    private static final long serialVersionUID = -2663361107262285638L;

    private JRadioButton useMatrixButton;
    private JRadioButton useRotationButton;

    private final JLabel[][] matrixLabels = new JLabel[3][3];
    private final JTextField[][] matrixTextFields = new JTextField[3][3];

    private final JLabel[] rotationLabels = new JLabel[3];
    private final JTextField[] rotationTextFields = new JTextField[3];

    public RotationPanel() {
        this.setBorder(BorderFactory.createCompoundBorder(
                BorderFactory.createTitledBorder("Rotation"),
                BorderFactory.createEmptyBorder(5, 5, 5, 5)));

        this.setLayout(new GridBagLayout());

        useMatrixButton = new JRadioButton("Use rotation matrix", true);
        useMatrixButton.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                if(SMG.getDataProvider().getCurrentSMO().useRotationMatrixInGUI) return;
                
                setEnabledMatrixGrid(true);
                setEnabledRotationGrid(false);
                SMG.getDataProvider().getCurrentSMO().useRotationMatrixInGUI = true;
            }
        });

        useRotationButton = new JRadioButton("Use Euler angles (Degrees)",
                false);
        useRotationButton.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                if(!SMG.getDataProvider().getCurrentSMO().useRotationMatrixInGUI) return;
                
                boolean useRotation = true;

                if (!SMG.getDataProvider().getCurrentSMO().rotationMatrix
                        .equals(DataProvider.dummySMO.rotationMatrix)) {
                    int result = JOptionPane.showConfirmDialog(
                            SMG.getGUI(),
                            "Using the Euler angles will revert all your changes to the rotation matrix. Continue? ",
                            "Warning", JOptionPane.YES_NO_OPTION,
                            JOptionPane.WARNING_MESSAGE);
                    useRotation = result == JOptionPane.YES_OPTION;
                }

                if (useRotation) {
                    setEnabledMatrixGrid(false);
                    setEnabledRotationGrid(true);
                    SMG.getDataProvider().getCurrentSMO().useRotationMatrixInGUI = false;
                } else {
                    useMatrixButton.setSelected(true);
                }
            }
        });

        ButtonGroup buttonGroup = new ButtonGroup();
        buttonGroup.add(useMatrixButton);
        buttonGroup.add(useRotationButton);

        GridBagConstraints c = new GridBagConstraints();
        c.fill = GridBagConstraints.HORIZONTAL;
        c.anchor = GridBagConstraints.PAGE_START;

        c.gridx = 0;
        c.gridwidth = 6;
        c.gridy = 0;
        this.add(useMatrixButton, c);

        createAndAddMatrixGrid();

        c.gridy = 4;
        this.add(Box.createRigidArea(new Dimension(10, 15)), c);

        c.gridy = 5;
        this.add(useRotationButton, c);

        createAndAddRotationGrid();

        setEnabledMatrixGrid(false);
        setEnabledRotationGrid(false);
        useMatrixButton.setEnabled(false);
        useRotationButton.setEnabled(false);
    }

    private void createAndAddMatrixGrid() {
        GridBagConstraints c = new GridBagConstraints();
        c.fill = GridBagConstraints.HORIZONTAL;
        c.anchor = GridBagConstraints.PAGE_START;
        c.gridwidth = 1;

        for (int y = 0; y < 3; ++y) {
            c.gridy = y + 1;
            for (int x = 0; x < 3; ++x) {
                c.gridx = x * 2;
                c.insets = new Insets(3, 3, 3, 3);
                c.weightx = 0;
                this.matrixLabels[y][x] = new JLabel("m" + y + x + ":",
                        SwingConstants.TRAILING);
                this.add(matrixLabels[y][x], c);
                c.gridx = x * 2 + 1;
                c.insets = new Insets(3, 3, 3, x == 2 ? 3 : 6);
                c.weightx = 1;
                matrixTextFields[y][x] = new JTextField("0");
                matrixTextFields[y][x].addActionListener(saveActionListener());
                matrixTextFields[y][x].addFocusListener(saveFocusListener());
                this.add(matrixTextFields[y][x], c);
            }
        }
    }

    private void createAndAddRotationGrid() {
        GridBagConstraints c = new GridBagConstraints();
        c.fill = GridBagConstraints.HORIZONTAL;
        c.anchor = GridBagConstraints.PAGE_START;
        c.gridwidth = 1;
        c.gridy = 6;

        this.rotationLabels[0] = new JLabel("x:", SwingConstants.TRAILING);
        this.rotationLabels[1] = new JLabel("y:", SwingConstants.TRAILING);
        this.rotationLabels[2] = new JLabel("z:", SwingConstants.TRAILING);

        for (int x = 0; x < 3; ++x) {
            c.gridx = x * 2;
            c.insets = new Insets(3, 3, 3, 3);
            c.weightx = 0;
            this.add(rotationLabels[x], c);
            c.gridx = x * 2 + 1;
            c.insets = new Insets(3, 3, 3, x == 2 ? 3 : 6);
            c.weightx = 1;
            rotationTextFields[x] = new JTextField("0");
            rotationTextFields[x].addActionListener(saveActionListener());
            rotationTextFields[x].addFocusListener(saveFocusListener());
            this.add(rotationTextFields[x], c);
        }
    }

    public void updateFull() {
        SMG.debugOutput("RotationPanel: updateFull");

        SemanticMapObject smo = SMG.getDataProvider().getCurrentSMO();
        if (smo == null) {
            smo = DataProvider.dummySMO;

            setEnabledMatrixGrid(false);
            setEnabledRotationGrid(false);
            useMatrixButton.setEnabled(false);
            useRotationButton.setEnabled(false);

        } else {
            if (smo.useRotationMatrixInGUI)
                useMatrixButton.setSelected(true);
            else
                useRotationButton.setSelected(true);

            setEnabledMatrixGrid(smo.useRotationMatrixInGUI);
            setEnabledRotationGrid(!smo.useRotationMatrixInGUI);
            useMatrixButton.setEnabled(true);
            useRotationButton.setEnabled(true);
        }

        for (int j = 0; j < 3; ++j) {
            for (int i = 0; i < 3; ++i) {
                matrixTextFields[j][i].setText(Double
                        .toString(smo.rotationMatrix.get(j, i)));
            }
            rotationTextFields[j].setText(Double.toString(smo.rotation.get(j)));
        }

    }

    private void setEnabledMatrixGrid(boolean enabled) {
        for (int j = 0; j < 3; ++j) {
            for (int i = 0; i < 3; ++i) {
                matrixTextFields[j][i].setEnabled(enabled);
                matrixLabels[j][i].setEnabled(enabled);
            }
        }
    }

    private void setEnabledRotationGrid(boolean enabled) {
        for (int i = 0; i < 3; ++i) {
            rotationTextFields[i].setEnabled(enabled);
            rotationLabels[i].setEnabled(enabled);
        }
    }

    private ActionListener saveActionListener() {
        return new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent arg0) {
                SMG.debugOutput("RotationPanel: saveActionListener - actionPerformed");
                saveData();
            }
        };
    }

    private FocusListener saveFocusListener() {
        return new FocusAdapter() {
            @Override
            public void focusLost(FocusEvent e) {
                SMG.debugOutput("RotationPanel: saveFocusListener - focusLost");
                saveData();
            }
            @Override
            public void focusGained(FocusEvent e) {
                if (e.getComponent() instanceof JTextField) {
                    JTextField t = (JTextField) (e.getComponent());
                    t.setSelectionStart(0);
                    t.setSelectionEnd(t.getText().length());
                }
            }
        };
    }

    private void saveData() {
        SMG.debugOutput("RotationPanel: saveData");

        if (useMatrixButton.isSelected()) {
            saveMatrix();
        } else {

            try {
                Vector3 rotationVector = new Vector3();
                for (int x = 0; x < 3; ++x) {
                    rotationVector
                            .set(x, Double.parseDouble(rotationTextFields[x]
                                    .getText()));
                }

                SMG.getDataProvider().getCurrentSMO().rotation = rotationVector;
                SMG.getDataProvider().getCurrentSMO().rotationMatrix
                        .rotateWithDegrees(rotationVector);

                for (int j = 0; j < 3; ++j) {
                    for (int i = 0; i < 3; ++i) {
                        SMG.getDataProvider().getCurrentSMO().rotationMatrix
                                .set(j, i, CommonMath
                                        .round(SMG.getDataProvider()
                                                .getCurrentSMO().rotationMatrix
                                                .get(j, i), 5));

                        matrixTextFields[j][i]
                                .setText(Double.toString(SMG.getDataProvider()
                                        .getCurrentSMO().rotationMatrix.get(j,
                                        i)));
                    }
                }
            } catch (NumberFormatException e) {
                SMG.debugOutput("RotationPanel: saveData - NumberFormatException, reseting rotation");
                updateFull();
            }

        }
    }

    private void saveMatrix() {
        SMG.debugOutput("RotationPanel: saveMatrix");

        try {
            Matrix3x3 newMatrix = new Matrix3x3();
            for (int y = 0; y < 3; ++y) {
                for (int x = 0; x < 3; ++x) {
                    newMatrix.set(y, x, Double
                            .parseDouble(matrixTextFields[y][x].getText()));
                }
            }

            SMG.getDataProvider().getCurrentSMO().rotationMatrix = newMatrix;

        } catch (NumberFormatException e) {
            SMG.debugOutput("RotationPanel: saveMatrix - NumberFormatException, reseting rotation");
            updateFull();
        }
    }
}
