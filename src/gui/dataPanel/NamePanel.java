/*
 * Copyright (c) 2012, Andreas Stolpmann <andisto@informatik.uni-bremen.de>
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the Intelligent Autonomous Systems Group/
 *       Universitaet Bremen nor the names of its contributors 
 *       may be used to endorse or promote products derived from this software 
 *       without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

package gui.dataPanel;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.util.concurrent.atomic.AtomicBoolean;

import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;

import data.SemanticMapObject;

import main.DataProvider;
import main.SMG;

public class NamePanel extends JPanel {
    private static final long serialVersionUID = -1696274691121729938L;

    private final JLabel nameLabel = new JLabel("Name:");
    private final JLabel typeLabel = new JLabel("Type:");

    private final JTextField nameTextField = new JTextField();
    private final JTextField typeTextField = new JTextField();
    
    private AtomicBoolean disableFocusListener;

    public NamePanel() {
        disableFocusListener = new AtomicBoolean(false);
        
        this.setLayout(new GridBagLayout());
        GridBagConstraints c = new GridBagConstraints();

        c.gridx = 0;
        c.gridy = 0;
        c.weightx = 0;
        c.fill = GridBagConstraints.HORIZONTAL;
        c.anchor = GridBagConstraints.PAGE_START;
        c.insets = new Insets(3, 5, 3, 3);
        this.add(nameLabel, c);

        c.gridx = 1;
        c.gridy = 0;
        c.weightx = 1;
        c.insets = new Insets(3, 3, 3, 5);
        this.add(nameTextField, c);

        c.gridx = 0;
        c.gridy = 1;
        c.weightx = 0;
        c.insets = new Insets(15, 5, 3, 3);
        this.add(typeLabel, c);

        c.gridx = 1;
        c.gridy = 1;
        c.weightx = 1;
        c.insets = new Insets(15, 3, 3, 5);
        this.add(typeTextField, c);

        nameTextField.addActionListener(saveActionListener());
        nameTextField.addFocusListener(saveFocusListener());
        typeTextField.addActionListener(saveActionListener());
        typeTextField.addFocusListener(saveFocusListener());

        this.setEnabledNamePanel(false);
    }
    
    private ActionListener saveActionListener() {
        return new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent arg0) {
                SMG.debugOutput("NamePanel: saveActionListener - actionPerformed");
                saveData();
            }
        };
    }

    private FocusListener saveFocusListener() {
        return new FocusAdapter() {
            @Override
            public void focusLost(FocusEvent e) {
                SMG.debugOutput("NamePanel: saveFocusListener - focusLost");
                if(disableFocusListener.get())
                    SMG.debugOutput("NamePanel: saveFocusListener - focusListener disabled");
                else
                    saveData();
            }
            @Override
            public void focusGained(FocusEvent e) {
                if(e.getComponent() instanceof JTextField) {
                    JTextField t = (JTextField)(e.getComponent());
                    t.setSelectionStart(0);
                    t.setSelectionEnd(t.getText().length());
                }
            }
        };
    }

    private void setEnabledNamePanel(boolean enabled) {
        this.nameLabel.setEnabled(enabled);
        this.nameTextField.setEnabled(enabled);
        this.typeLabel.setEnabled(enabled);
        this.typeTextField.setEnabled(enabled);
    }

    public void updateFull() {
        SMG.debugOutput("NamePanel: updateFull");

        SemanticMapObject smo = SMG.getDataProvider().getCurrentSMO();
        if (smo == null) {
            smo = DataProvider.dummySMO;
            this.setEnabledNamePanel(false);
        } else {
            this.setEnabledNamePanel(true);
        }

        nameTextField.setText(smo.name);
        typeTextField.setText(smo.type);
    }

    private void saveData() {
        SMG.debugOutput("NamePanel: saveData");

        String newName = nameTextField.getText();

        if (!SMG.getDataProvider().getCurrentSMO().name.equals(newName)) {

            if (SMG.getDataProvider().renameSMO(newName)) {
                SMG.debugOutput("NamePanel: saveData - rename successfull");
                SMG.getGUI().renamedCurrentSMO();
            } else {
                SMG.debugOutput("NamePanel: saveData - rename failed");
                
                SMG.debugOutput("NamePanel: saveData - disabling focusListener");
                disableFocusListener.set(true);
                
                nameTextField.setText(SMG.getDataProvider().getCurrentSMO().name);
                JOptionPane.showMessageDialog(SMG.getGUI(),
                        "An object with that name allready exists!", "Error",
                        JOptionPane.ERROR_MESSAGE);
                
                SMG.debugOutput("NamePanel: saveData - enabling focusListener");
                disableFocusListener.set(false);
            }
        } else {
            SMG.debugOutput("NamePanel: saveData - rename not necessary");
        }

        SMG.getDataProvider().getCurrentSMO().type = typeTextField.getText();
    }

}