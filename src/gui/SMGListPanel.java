/*
 * Copyright (c) 2012, Andreas Stolpmann <andisto@informatik.uni-bremen.de>
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the Intelligent Autonomous Systems Group/
 *       Universitaet Bremen nor the names of its contributors 
 *       may be used to endorse or promote products derived from this software 
 *       without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

package gui;

import java.awt.BorderLayout;

import javax.swing.AbstractListModel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.ListSelectionModel;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

import main.SMG;

public class SMGListPanel extends JPanel implements ListSelectionListener {
    private static final long serialVersionUID = 1490097905990008733L;

    private JList list;
    private SMGListModel listModel;

    public SMGListPanel() {
        super(new BorderLayout(5, 5));

        listModel = new SMGListModel();

        list = new JList(listModel);
        list.addListSelectionListener(this);
        list.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        list.setLayoutOrientation(JList.VERTICAL);

        this.add(new JScrollPane(list), BorderLayout.CENTER);
    }

    public void addedNewSMO() {
        listModel.elementAdded();
    }

    protected void removeCurrentSMO() {
        listModel.elementRemoved();
    }
    
    protected void updateCurrentSMO() {
       listModel.updateCurrent();
    }
    
    protected void renamedCurrentSMO() {
        listModel.renamedCurrent();
    }
    
    protected void updateFull() {
        listModel.updateFull();
    }
    
    @Override
    public void valueChanged(ListSelectionEvent e) {
        if (!e.getValueIsAdjusting()) {
            SMG.getDataProvider().setCurrentSMO(list.getSelectedIndex());
            SMG.getGUI().updateCurrentSMO();
        }
    }

    private class SMGListModel extends AbstractListModel {
        private static final long serialVersionUID = 7128868400565848643L;
                
        public void elementAdded() {
            this.fireIntervalAdded(this, getSize() - 1, getSize());
            list.setSelectedIndex(SMG.getDataProvider().getCurrentSMOIndex());
            SMG.getGUI().updateCurrentSMO();
        }

        public void elementRemoved() {
            this.fireIntervalRemoved(this, 0, list.getSelectedIndex());
            list.setSelectedIndex(0);
        }
        
        public void updateFull() {
            this.fireContentsChanged(this, 0, getSize());
        }

        public void updateCurrent() {
            this.fireContentsChanged(this, list.getSelectedIndex(), list.getSelectedIndex() + 1);
            list.setSelectedIndex(SMG.getDataProvider().getCurrentSMOIndex());
        }
        
        public void renamedCurrent() {
            this.fireContentsChanged(this, 0, getSize());
            list.setSelectedIndex(SMG.getDataProvider().getCurrentSMOIndex());
        }

        @Override
        public Object getElementAt(int index) {
            return SMG.getDataProvider().getCurrentSMOList().get(index);
        }

        @Override
        public int getSize() {
            return SMG.getDataProvider().getCurrentSMOList().size();
        }
    }
}