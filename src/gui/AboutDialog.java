/*
 * Copyright (c) 2012, Andreas Stolpmann <andisto@informatik.uni-bremen.de>
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the Intelligent Autonomous Systems Group/
 *       Universitaet Bremen nor the names of its contributors 
 *       may be used to endorse or promote products derived from this software 
 *       without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
package gui;

import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;

import main.SMG;

@SuppressWarnings("serial")
public class AboutDialog extends JDialog {

    public AboutDialog(JFrame parent) {
        super(parent, "About " + SMG.NAME, true);

        setLayout(new BoxLayout(getContentPane(), BoxLayout.Y_AXIS));

        add(Box.createRigidArea(new Dimension(0, 15)));

        JLabel label1 = new JLabel(SMG.NAME + " Version "
                + SMG.VERSION);
        label1.setAlignmentX(0.5f);
        add(label1);

        add(Box.createRigidArea(new Dimension(0, 15)));

        JTextArea textArea = new JTextArea();
        textArea.setEditable(false);
        textArea.setAlignmentX(0.5f);

        textArea.append("\n");
        textArea.append(" Copyright (c) 2012, Andreas Stolpmann <andisto@informatik.uni-bremen.de>\n");
        textArea.append(" All rights reserved.\n");
        textArea.append("\n");
        textArea.append(" Redistribution and use in source and binary forms, with or without\n");
        textArea.append(" modification, are permitted provided that the following conditions are met:\n");
        textArea.append("\n");
        textArea.append("     * Redistributions of source code must retain the above copyright\n");
        textArea.append("       notice, this list of conditions and the following disclaimer.\n");
        textArea.append("     * Redistributions in binary form must reproduce the above copyright\n");
        textArea.append("       notice, this list of conditions and the following disclaimer in the\n");
        textArea.append("       documentation and/or other materials provided with the distribution.\n");
        textArea.append("     * Neither the name of the Intelligent Autonomous Systems Group/\n");
        textArea.append("       Universitaet Bremen nor the names of its contributors\n");
        textArea.append("       may be used to endorse or promote products derived from this software\n");
        textArea.append("       without specific prior written permission.\n");
        textArea.append("\n");
        textArea.append(" THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS \"AS IS\"\n");
        textArea.append(" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE\n");
        textArea.append(" IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE\n");
        textArea.append(" ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE\n");
        textArea.append(" LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR\n");
        textArea.append(" CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF\n");
        textArea.append(" SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS\n");
        textArea.append(" INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN\n");
        textArea.append(" CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)\n");
        textArea.append(" ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE\n");
        textArea.append(" POSSIBILITY OF SUCH DAMAGE.\n");

        add(new JScrollPane(textArea));

        textArea.setSelectionStart(0);
        textArea.setSelectionEnd(0);

        add(Box.createRigidArea(new Dimension(0, 15)));

        JButton close = new JButton("Close");
        close.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent event) {
                dispose();
            }
        });

        close.setAlignmentX(0.5f);
        add(close);

        add(Box.createRigidArea(new Dimension(0, 15)));

        setModalityType(ModalityType.APPLICATION_MODAL);
        setDefaultCloseOperation(DISPOSE_ON_CLOSE);
        setSize(645, 400);

        setResizable(false);

        setLocationRelativeTo(parent);
        setVisible(true);
    }
}
