/*
 * Copyright (c) 2012, Andreas Stolpmann <andisto@informatik.uni-bremen.de>
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the Intelligent Autonomous Systems Group/
 *       Universitaet Bremen nor the names of its contributors 
 *       may be used to endorse or promote products derived from this software 
 *       without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
package gui;

import gui.dataPanel.SMGEditorPanel;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.EventQueue;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JSplitPane;
import javax.swing.KeyStroke;

import main.SMG;

public class SMGGUI extends JPanel {
    private static final long serialVersionUID = 8456892429632799611L;
    
    private static final int MAIN_WINDOW_WIDTH = 550;
    private static final int MAIN_WINDOW_HEIGHT = 450;
    
    private static final int LEFT_PANEL_WIDTH = 140;
    private static final int LEFT_PANEL_HEIGHT = 400;
    
    private static final int RIGHT_PANEL_WIDTH = 300;
    private static final int RIGHT_PANEL_HEIGHT = 400;    
    

    private final SMGListPanel listPanel;
    private final SMGButtonPanel buttonPanel;
    private final SMGEditorPanel editorPanel;

    public SMGGUI() {
        super(new BorderLayout());
        this.setOpaque(true);

        final JPanel leftPanel = new JPanel(new GridBagLayout());
        final GridBagConstraints c = new GridBagConstraints();

        this.listPanel = new SMGListPanel();
        this.buttonPanel = new SMGButtonPanel(listPanel);
        this.editorPanel = new SMGEditorPanel();

        c.fill = GridBagConstraints.BOTH;
        c.insets = new Insets(5, 5, 0, 5);
        c.weightx = 1;
        c.weighty = 1;
        c.gridy = 0;
        leftPanel.add(listPanel, c);

        c.insets = new Insets(0, 5, 5, 5);
        c.weighty = 0;
        c.gridy = 1;
        leftPanel.add(buttonPanel, c);

        leftPanel.setMinimumSize(new Dimension(LEFT_PANEL_WIDTH, LEFT_PANEL_HEIGHT));
        editorPanel.setMinimumSize(new Dimension(RIGHT_PANEL_WIDTH, RIGHT_PANEL_HEIGHT));

        final JSplitPane splitPane = new JSplitPane(
                JSplitPane.HORIZONTAL_SPLIT, leftPanel, editorPanel);
        splitPane.setContinuousLayout(true);

        this.add(splitPane, BorderLayout.CENTER);
        this.setBorder(BorderFactory.createEmptyBorder(5, 5, 5, 5));

        EventQueue.invokeLater(new Runnable() {
            public void run() {
                new MainFrame(SMGGUI.this).setVisible(true);
                splitPane.setDividerLocation(0.3);
            }
        });
    }
    
    
    public void updateFull() {
        SMG.debugOutput("GUI: updateFull");
        
        listPanel.updateFull();
        
        if(SMG.getDataProvider().getCurrentSMOList().isEmpty()) 
            buttonPanel.enableRemoveButton(false);
        else
            buttonPanel.enableRemoveButton(true);
    }
    
    public void addedNewSMO() {
        SMG.debugOutput("GUI: addedNewSMO");
        
        listPanel.addedNewSMO();        
        buttonPanel.enableRemoveButton(true);
    }
    
    public void removeCurrentSMO() {
        SMG.debugOutput("GUI: removeCurrentSMO");
        
        listPanel.removeCurrentSMO();
        
        if(SMG.getDataProvider().getCurrentSMOList().isEmpty()) {
            buttonPanel.enableRemoveButton(false);
        }
    }
    
    public void updateCurrentSMO() {
        SMG.debugOutput("GUI: updateCurrentSMO");
        
        listPanel.updateCurrentSMO();
        editorPanel.updateFull();
    }
    

    public void renamedCurrentSMO() {
        SMG.debugOutput("GUI: renamedCurrentSMO");
        
        listPanel.renamedCurrentSMO();
    }

    private class MainFrame extends JFrame {
        private static final long serialVersionUID = 7764729193018168226L;

        public MainFrame(JPanel contentPane) {
            super(SMG.NAME + " v" + SMG.VERSION);

            this.setSize(new Dimension(MAIN_WINDOW_WIDTH, MAIN_WINDOW_HEIGHT));
            this.setMinimumSize(new Dimension(MAIN_WINDOW_WIDTH, MAIN_WINDOW_HEIGHT));
            // this.setPreferredSize(new Dimension(800, 600));
            this.setResizable(true);
            this.setLocationRelativeTo(null);
            this.setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
            this.addWindowListener(new WindowAdapter() {
                @Override
                public void windowClosing(WindowEvent e) {
                    SMG.getSMG().close();
                }
            });

            this.add(contentPane);
            this.setJMenuBar(createMenu());
        }

        private JMenuBar createMenu() {
            JMenuBar menu = new JMenuBar();

            JMenu fileMenu = new JMenu("File");
            fileMenu.setMnemonic(KeyEvent.VK_F);
            JMenuItem newItem = new JMenuItem("New Project", KeyEvent.VK_N);
            newItem.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_N,
                    ActionEvent.ALT_MASK));
            newItem.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    SMG.getDataProvider().newProject();
                }
            });
            fileMenu.add(newItem);

            fileMenu.addSeparator();
            JMenuItem saveItem = new JMenuItem("Save", KeyEvent.VK_S);
            saveItem.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_S,
                    ActionEvent.CTRL_MASK));
            saveItem.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    SMG.getDataProvider().save();
                }
            });
            fileMenu.add(saveItem);
            
            JMenuItem saveAsItem = new JMenuItem("Save As");
            saveAsItem.setMnemonic(KeyEvent.VK_A);
            saveAsItem.setDisplayedMnemonicIndex(5);
            saveAsItem.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    SMG.getDataProvider().saveAs();
                }
            });
            fileMenu.add(saveAsItem);

            JMenuItem loadItem = new JMenuItem("Load", KeyEvent.VK_L);
            loadItem.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_L,
                    ActionEvent.CTRL_MASK));
            loadItem.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    SMG.getDataProvider().load();
                }
            });
            fileMenu.add(loadItem);

            fileMenu.addSeparator();
            JMenuItem exportItem = new JMenuItem("Export", KeyEvent.VK_E);
            exportItem.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_E,
                    ActionEvent.ALT_MASK));
            exportItem.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    SMG.getDataProvider().export();
                }
            });
            fileMenu.add(exportItem);

            fileMenu.addSeparator();
            JMenuItem closeItem = new JMenuItem("Exit", KeyEvent.VK_X);
            closeItem.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    SMG.getSMG().close();
                }
            });
            fileMenu.add(closeItem);

            menu.add(fileMenu);

            menu.add(Box.createHorizontalGlue());

            JMenu aboutButton = new JMenu("About");
            aboutButton.addMouseListener(new MouseAdapter() {
                @Override
                public void mouseReleased(MouseEvent e) {
                    if (e.getButton() == MouseEvent.BUTTON1) {
                        new AboutDialog(MainFrame.this);
                    }
                }

                @Override
                public void mouseClicked(MouseEvent e) {
                    if (e.getButton() == MouseEvent.BUTTON1) {
                        new AboutDialog(MainFrame.this);
                    }
                }
            });
            menu.add(aboutButton);
            
            return menu;
        }
    }
}