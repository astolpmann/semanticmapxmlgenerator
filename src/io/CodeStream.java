/*
 * Copyright (c) 2012, Andreas Stolpmann <andisto@informatik.uni-bremen.de>
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the Intelligent Autonomous Systems Group/
 *       Universitaet Bremen nor the names of its contributors 
 *       may be used to endorse or promote products derived from this software 
 *       without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
package io;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintStream;

public class CodeStream extends PrintStream {

    private int indentionStep;
    private int indention;

    public CodeStream(int indentionStep) {
        super(System.out);
        this.indentionStep = indentionStep;
        this.indention = 0;
    }

    public CodeStream(File file, int indentionStep)
            throws FileNotFoundException {
        super(file);
        this.indentionStep = indentionStep;
        this.indention = 0;
    }

    @Override
    public void print(String line) {
        for (int i = 0; i < indention; ++i)
            super.print(" ");

        super.print(line);
    }

    @Override
    public void println(String line) {
        this.print(line);
        super.println();
    }

    public void indent() {
        indention += indentionStep;
    }

    public void unindent() {
        indention -= indentionStep;
        if (indention < 0)
            indention = 0;
    }
}
