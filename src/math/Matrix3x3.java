/*
 * Copyright (c) 2012, Andreas Stolpmann <andisto@informatik.uni-bremen.de>
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the Intelligent Autonomous Systems Group/
 *       Universitaet Bremen nor the names of its contributors 
 *       may be used to endorse or promote products derived from this software 
 *       without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
package math;

import java.io.Serializable;

public class Matrix3x3 implements Serializable, Cloneable{
    private static final long serialVersionUID = 3584624958390505413L;
    
    public double m00, m01, m02;
    public double m10, m11, m12;
    public double m20, m21, m22;

    public Matrix3x3() {
        this.m00 = this.m11 = this.m22 = 1;
        this.m01 = this.m02 = this.m10 = 0;
        this.m12 = this.m20 = this.m21 = 0;
    }

    public Matrix3x3(double m00, double m01, double m02, double m10,
            double m11, double m12, double m20, double m21, double m22) {
        this.m00 = m00;
        this.m01 = m01;
        this.m02 = m02;
        this.m10 = m10;
        this.m11 = m11;
        this.m12 = m12;
        this.m20 = m20;
        this.m21 = m21;
        this.m22 = m22;
    }

    public Matrix3x3 rotate(double x, double y, double z) {

        Matrix3x3 mX = new Matrix3x3();
        mX.m11 = Math.cos(x);
        mX.m12 = -Math.sin(x);
        mX.m21 = Math.sin(x);
        mX.m22 = Math.cos(x);

        Matrix3x3 mY = new Matrix3x3();
        mY.m00 = Math.cos(y);
        mY.m02 = Math.sin(y);
        mY.m20 = -Math.sin(y);
        mY.m22 = Math.cos(y);

        Matrix3x3 mYX = mY.multiply(mX);

        Matrix3x3 mZ = new Matrix3x3();
        mZ.m00 = Math.cos(z);
        mZ.m01 = -Math.sin(z);
        mZ.m10 = Math.sin(z);
        mZ.m11 = Math.cos(z);

        Matrix3x3 mZYX = mZ.multiply(mYX);

        this.m00 = mZYX.m00;
        this.m01 = mZYX.m01;
        this.m02 = mZYX.m02;
        this.m10 = mZYX.m10;
        this.m11 = mZYX.m11;
        this.m12 = mZYX.m12;
        this.m20 = mZYX.m20;
        this.m21 = mZYX.m21;
        this.m22 = mZYX.m22;
        
        return this;
    }
    
    public Matrix3x3 rotate(Vector3 rotationVector) {
        return rotate(rotationVector.x, rotationVector.y, rotationVector.z);
    }
    
    public Matrix3x3 rotateWithDegrees(Vector3 rotationVector) {
        return rotate(Math.toRadians(rotationVector.x), Math.toRadians(rotationVector.y), Math.toRadians(rotationVector.z));
    }

    public Matrix3x3 multiply(Matrix3x3 other) {
        Matrix3x3 resultMatrix = new Matrix3x3();

        for (int i = 0; i < 3; ++i) {
            for (int j = 0; j < 3; ++j) {
                double result = 0;

                for (int k = 0; k < 3; ++k) {
                    result += get(i, k) * other.get(k, j);
                }

                resultMatrix.set(i, j, result);
            }
        }

        return resultMatrix;
    }

    public Vector3 multiply(Vector3 vector) {
        Vector3 resultVector = new Vector3();

        for (int i = 0; i < 3; ++i) {
            double result = 0;
            for (int j = 0; j < 3; ++j) {
                result += get(i, j) * vector.get(j);
            }
            resultVector.set(i, result);
        }

        return resultVector;
    }

    public Double get(int j, int i) {
        switch (j) {
            case 0 :
                switch (i) {
                    case 0 :
                        return m00;
                    case 1 :
                        return m01;
                    case 2 :
                        return m02;
                    default :
                        assert (false);
                        return null;
                }
            case 1 :
                switch (i) {
                    case 0 :
                        return m10;
                    case 1 :
                        return m11;
                    case 2 :
                        return m12;
                    default :
                        assert (false);
                        return null;
                }
            case 2 :
                switch (i) {
                    case 0 :
                        return m20;
                    case 1 :
                        return m21;
                    case 2 :
                        return m22;
                    default :
                        assert (false);
                        return null;
                }
            default :
                assert (false);
                return null;
        }
    }

    public void set(int j, int i, Double value) {
        switch (j) {
            case 0 :
                switch (i) {
                    case 0 :
                        m00 = value;
                        break;
                    case 1 :
                        m01 = value;
                        break;
                    case 2 :
                        m02 = value;
                        break;
                    default :
                        assert (false);
                }
                break;
            case 1 :
                switch (i) {
                    case 0 :
                        m10 = value;
                        break;
                    case 1 :
                        m11 = value;
                        break;
                    case 2 :
                        m12 = value;
                        break;
                    default :
                        assert (false);
                }
                break;
            case 2 :
                switch (i) {
                    case 0 :
                        m20 = value;
                        break;
                    case 1 :
                        m21 = value;
                        break;
                    case 2 :
                        m22 = value;
                        break;
                    default :
                        assert (false);
                }
                break;
            default :
                assert (false);
        }
    }
    
    @Override
    public Matrix3x3 clone() {
        return new Matrix3x3(m00, m01, m02, m10, m11, m12, m20, m21, m22);
    }
    
    @Override
    public boolean equals(Object other) {
        if (other == null || !other.getClass().equals(this.getClass()))
            return false;
        
        Matrix3x3 otherMatrix = (Matrix3x3)other;

        for(int j = 0; j < 3; ++j) {
            for(int i = 0; i < 3; ++i) {
                if(!get(j, i).equals(otherMatrix.get(j, i)))
                    return false;
            }
        }
        
        return true;
    }
}