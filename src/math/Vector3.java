/*
 * Copyright (c) 2012, Andreas Stolpmann <andisto@informatik.uni-bremen.de>
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the Intelligent Autonomous Systems Group/
 *       Universitaet Bremen nor the names of its contributors 
 *       may be used to endorse or promote products derived from this software 
 *       without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
package math;

import java.io.Serializable;
import java.util.Iterator;
import java.util.NoSuchElementException;

public class Vector3 implements Serializable, Cloneable, Iterable<Double> {
    private static final long serialVersionUID = 2204671395261731855L;

    public Double x;
    public Double y;
    public Double z;

    public Vector3() {
        this(0, 0, 0);
    }

    public Vector3(double x, double y, double z) {
        this.x = x;
        this.y = y;
        this.z = z;
    }

    public Double get(int i) {
        switch (i) {
            case 0 :
                return x;
            case 1 :
                return y;
            case 2 :
                return z;
            default :
                assert (false);
                return null;
        }
    }

    public void set(int i, Double value) {
        switch (i) {
            case 0 :
                x = value;
                break;
            case 1 :
                y = value;
                break;
            case 2 :
                z = value;
                break;
            default :
                assert (false);
        }
    }

    @Override
    public Vector3 clone() {
        return new Vector3(x, y, z);
    }

    @Override
    public Iterator<Double> iterator() {
        return new MyIterator<Double>(new Double[]{x, y, z});
    }

    private class MyIterator<E> implements Iterator<E> {

        private E[] arr;
        private int position;

        public MyIterator(E[] elements) {
            this.arr = elements;
            this.position = -1;
        }

        public boolean hasNext() {
            return this.position + 1 < this.arr.length;
        }

        public E next() throws NoSuchElementException {

            if (!hasNext()) {
                throw new NoSuchElementException("No more elements");
            }
            return this.arr[++this.position];
        }

        public void remove() throws UnsupportedOperationException {
            throw new UnsupportedOperationException(
                    "Operation is not supported");
        }
    }

}