/*
 * Copyright (c) 2012, Andreas Stolpmann <andisto@informatik.uni-bremen.de>
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the Intelligent Autonomous Systems Group/
 *       Universitaet Bremen nor the names of its contributors 
 *       may be used to endorse or promote products derived from this software 
 *       without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
package data;

import io.CodeStream;

import java.io.Serializable;

import math.Matrix3x3;
import math.Vector3;

public class SemanticMapObject
        implements
            Serializable,
            Comparable<SemanticMapObject> {

    private static final long serialVersionUID = 8318409802785777122L;
    public static int nextID = 0;
    
    public final int id;

    public String name;
    public String type;

    public Vector3 translation;
    public Vector3 size;

    public Matrix3x3 rotationMatrix;    
    public Vector3 rotation;
    
    public boolean useRotationMatrixInGUI;
    

    public SemanticMapObject() {
        id = nextID++;
        
        this.name = "";
        this.type = "";
        
        this.translation = new Vector3();
        this.size = new Vector3();
        this.rotationMatrix = new Matrix3x3();
        this.rotation = new Vector3();
        
        this.useRotationMatrixInGUI = true;
    }
    
    public void copyValues(SemanticMapObject otherSMO) {
        this.name = otherSMO.name;
        this.type = otherSMO.type;
        
        this.translation = otherSMO.translation.clone();
        this.size = otherSMO.size.clone();
        this.rotationMatrix = otherSMO.rotationMatrix.clone();
        this.rotation = otherSMO.rotation.clone();

        this.useRotationMatrixInGUI = otherSMO.useRotationMatrixInGUI;
    }

    @Override
    public String toString() {
        return name;
    }

    @Override
    public boolean equals(Object other) {
        if (other == null || !other.getClass().equals(this.getClass()))
            return false;

        return this.id == ((SemanticMapObject) other).id;
        //return this.name.equalsIgnoreCase(((SemanticMapObject) other).name);
    }

    @Override
    public int compareTo(SemanticMapObject otherSmo) {
        return this.name.compareToIgnoreCase(otherSmo.name);
    }

    public void generateCode(CodeStream out) {
        out.indent();
        out.println("");
        out.println("");
        out.println("");
        out.println("");
        out.println("<owl:NamedIndividual rdf:about=\"http://ias.cs.tum.edu/kb/knowrob.owl#RotationMatrix3D_"
                + this.name + "\">");
        out.indent();
        out.println("<rdf:type rdf:resource=\"http://ias.cs.tum.edu/kb/knowrob.owl#RotationMatrix3D\"/>");
        out.println("<knowrob:m00 rdf:datatype=\"http://www.w3.org/2001/XMLSchema#double\">"
                + this.rotationMatrix.m00 + "</knowrob:m00>");
        out.println("<knowrob:m01 rdf:datatype=\"http://www.w3.org/2001/XMLSchema#double\">"
                + this.rotationMatrix.m01 + "</knowrob:m01>");
        out.println("<knowrob:m02 rdf:datatype=\"http://www.w3.org/2001/XMLSchema#double\">"
                + this.rotationMatrix.m02 + "</knowrob:m02>");
        out.println("<knowrob:m10 rdf:datatype=\"http://www.w3.org/2001/XMLSchema#double\">"
                + this.rotationMatrix.m10 + "</knowrob:m10>");
        out.println("<knowrob:m11 rdf:datatype=\"http://www.w3.org/2001/XMLSchema#double\">"
                + this.rotationMatrix.m11 + "</knowrob:m11>");
        out.println("<knowrob:m12 rdf:datatype=\"http://www.w3.org/2001/XMLSchema#double\">"
                + this.rotationMatrix.m12 + "</knowrob:m12>");
        out.println("<knowrob:m20 rdf:datatype=\"http://www.w3.org/2001/XMLSchema#double\">"
                + this.rotationMatrix.m20 + "</knowrob:m20>");
        out.println("<knowrob:m21 rdf:datatype=\"http://www.w3.org/2001/XMLSchema#double\">"
                + this.rotationMatrix.m21 + "</knowrob:m21>");
        out.println("<knowrob:m22 rdf:datatype=\"http://www.w3.org/2001/XMLSchema#double\">"
                + this.rotationMatrix.m22 + "</knowrob:m22>");
        out.println("");
        out.println("<knowrob:m03 rdf:datatype=\"http://www.w3.org/2001/XMLSchema#double\">"
                + this.translation.x + "</knowrob:m03>");
        out.println("<knowrob:m13 rdf:datatype=\"http://www.w3.org/2001/XMLSchema#double\">"
                + this.translation.y + "</knowrob:m13>");
        out.println("<knowrob:m23 rdf:datatype=\"http://www.w3.org/2001/XMLSchema#double\">"
                + this.translation.z + "</knowrob:m23>");
        out.println("");
        out.println("<knowrob:m30 rdf:datatype=\"http://www.w3.org/2001/XMLSchema#double\">0.0</knowrob:m30>");
        out.println("<knowrob:m31 rdf:datatype=\"http://www.w3.org/2001/XMLSchema#double\">0.0</knowrob:m31>");
        out.println("<knowrob:m32 rdf:datatype=\"http://www.w3.org/2001/XMLSchema#double\">0.0</knowrob:m32>");
        out.println("<knowrob:m33 rdf:datatype=\"http://www.w3.org/2001/XMLSchema#double\">1.0</knowrob:m33>");
        out.unindent();
        out.println("</owl:NamedIndividual>");
        out.println("");
        out.println("");
        out.println("<owl:NamedIndividual rdf:about=\"http://ias.cs.tum.edu/kb/knowrob.owl#SemanticMapPerception_"
                + this.name + "\">");
        out.indent();
        out.println("<rdf:type rdf:resource=\"http://ias.cs.tum.edu/kb/knowrob.owl#SemanticMapPerception\"/>");
        out.println("<knowrob:startTime rdf:resource=\"http://ias.cs.tum.edu/kb/ias_semantic_map.owl#timepoint_1344842159\"/>");
        out.println("<knowrob:eventOccursAt rdf:resource=\"http://ias.cs.tum.edu/kb/knowrob.owl#RotationMatrix3D_"
                + this.name + "\"/>");
        out.println("<knowrob:objectActedOn rdf:resource=\"http://ias.cs.tum.edu/kb/knowrob.owl#"
                + this.name + "\"/>");
        out.unindent();
        out.println("</owl:NamedIndividual>");
        out.println("");
        out.println("");
        out.println("<owl:NamedIndividual rdf:about=\"http://ias.cs.tum.edu/kb/knowrob.owl#"
                + this.name + "\">");
        out.indent();
        out.println("<rdf:type rdf:resource=\"http://ias.cs.tum.edu/kb/knowrob.owl#"
                + this.type + "\"/>");
        out.println("<knowrob:widthOfObject rdf:datatype=\"http://www.w3.org/2001/XMLSchema#double\">"
                + this.size.x + "</knowrob:widthOfObject>");
        out.println("<knowrob:heightOfObject rdf:datatype=\"http://www.w3.org/2001/XMLSchema#double\">"
                + this.size.y + "</knowrob:heightOfObject>");
        out.println("<knowrob:depthOfObject rdf:datatype=\"http://www.w3.org/2001/XMLSchema#double\">"
                + this.size.z + "</knowrob:depthOfObject>");
        out.println("<knowrob:describedInMap rdf:resource=\"http://ias.cs.tum.edu/kb/ias_semantic_map.owl#SemanticEnvironmentMap0\"/>");
        out.unindent();
        out.println("</owl:NamedIndividual>");
        out.unindent();
    }
}