/*
 * Copyright (c) 2012, Andreas Stolpmann <andisto@informatik.uni-bremen.de>
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the Intelligent Autonomous Systems Group/
 *       Universitaet Bremen nor the names of its contributors 
 *       may be used to endorse or promote products derived from this software 
 *       without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
package data;

import io.CodeStream;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class SMGProject implements Serializable{

    private static final long serialVersionUID = -8986429780288253485L;

    public String name;
    public String path;

    public List<SemanticMapObject> smoList;

    public SMGProject() {
        this("new project");
    }

    public SMGProject(String name) {
        this.name = name;
        this.path = null;
        smoList = new ArrayList<SemanticMapObject>();
    }


    public void generateCode(CodeStream out) {
        out.println("<?xml version=\"1.0\"?>");
        out.println("<rdf:RDF xmlns=\"http://ias.cs.tum.edu/kb/ias_semantic_map.owl#\"");
        out.indent();
        out.println("xml:base=\"http://ias.cs.tum.edu/kb/ias_semantic_map.owl\"");
        out.println("xmlns:rdfs=\"http://www.w3.org/2000/01/rdf-schema#\"");
        out.println("xmlns:map=\"http://ias.cs.tum.edu/kb/ias_semantic_map.owl#\"");
        out.println("xmlns:owl=\"http://www.w3.org/2002/07/owl#\"");
        out.println("xmlns:xsd=\"http://www.w3.org/2001/XMLSchema#\"");
        out.println("xmlns:rdf=\"http://www.w3.org/1999/02/22-rdf-syntax-ns#\"");
        out.println("xmlns:knowrob=\"http://ias.cs.tum.edu/kb/knowrob.owl#\">");
        out.println("<owl:Ontology rdf:about=\"http://ias.cs.tum.edu/kb/ias_semantic_map.owl#\">");
        out.println("<owl:imports rdf:resource=\"http://ias.cs.tum.edu/kb/knowrob.owl#\"/>");
        out.println("</owl:Ontology>");
        out.println("");
        out.println("");
        out.println("");
        out.println("<!--");
        out.println("///////////////////////////////////////////////////////////////////////////////////////");
        out.println("//");
        out.println("// Object Properties");
        out.println("//");
        out.println("///////////////////////////////////////////////////////////////////////////////////////");
        out.println(" -->");
        out.println("");
        out.println("");
        out.println("<!-- http://ias.cs.tum.edu/kb/knowrob.owl#describedInMap -->");
        out.println("");
        out.println("<owl:ObjectProperty rdf:about=\"http://ias.cs.tum.edu/kb/knowrob.owl#describedInMap\"/>");
        out.println("");
        out.println("");
        out.println("<!-- http://ias.cs.tum.edu/kb/knowrob.owl#eventOccursAt -->");
        out.println("");
        out.println("<owl:ObjectProperty rdf:about=\"http://ias.cs.tum.edu/kb/knowrob.owl#eventOccursAt\"/>");
        out.println("");
        out.println("");
        out.println("<!-- http://ias.cs.tum.edu/kb/knowrob.owl#objectActedOn -->");
        out.println("");
        out.println("<owl:ObjectProperty rdf:about=\"http://ias.cs.tum.edu/kb/knowrob.owl#objectActedOn\"/>");
        out.println("");
        out.println("");
        out.println("<!-- http://ias.cs.tum.edu/kb/knowrob.owl#startTime -->");
        out.println("");
        out.println("<owl:ObjectProperty rdf:about=\"http://ias.cs.tum.edu/kb/knowrob.owl#startTime\"/>");
        out.println("");
        out.println("");
        out.println("");
        out.println("");
        out.println("<!--");
        out.println("///////////////////////////////////////////////////////////////////////////////////////");
        out.println("//");
        out.println("// Data properties");
        out.println("//");
        out.println("///////////////////////////////////////////////////////////////////////////////////////");
        out.println(" -->");
        out.println("");
        out.println("");
        out.println("<!-- http://ias.cs.tum.edu/kb/knowrob.owl#depthOfObject -->");
        out.println("");
        out.println("<owl:DatatypeProperty rdf:about=\"http://ias.cs.tum.edu/kb/knowrob.owl#depthOfObject\"/>");
        out.println("");
        out.println("");
        out.println("<!-- http://ias.cs.tum.edu/kb/knowrob.owl#heightOfObject -->");
        out.println("");
        out.println("<owl:DatatypeProperty rdf:about=\"http://ias.cs.tum.edu/kb/knowrob.owl#heightOfObject\"/>");
        out.println("");
        out.println("");
        out.println("<!-- http://ias.cs.tum.edu/kb/knowrob.owl#m00 -->");
        out.println("");
        out.println("<owl:DatatypeProperty rdf:about=\"http://ias.cs.tum.edu/kb/knowrob.owl#m00\"/>");
        out.println("");
        out.println("");
        out.println("<!-- http://ias.cs.tum.edu/kb/knowrob.owl#m01 -->");
        out.println("");
        out.println("<owl:DatatypeProperty rdf:about=\"http://ias.cs.tum.edu/kb/knowrob.owl#m01\"/>");
        out.println("");
        out.println("");
        out.println("<!-- http://ias.cs.tum.edu/kb/knowrob.owl#m02 -->");
        out.println("");
        out.println("<owl:DatatypeProperty rdf:about=\"http://ias.cs.tum.edu/kb/knowrob.owl#m02\"/>");
        out.println("");
        out.println("");
        out.println("<!-- http://ias.cs.tum.edu/kb/knowrob.owl#m03 -->");
        out.println("");
        out.println("<owl:DatatypeProperty rdf:about=\"http://ias.cs.tum.edu/kb/knowrob.owl#m03\"/>");
        out.println("");
        out.println("");
        out.println("<!-- http://ias.cs.tum.edu/kb/knowrob.owl#m10 -->");
        out.println("");
        out.println("<owl:DatatypeProperty rdf:about=\"http://ias.cs.tum.edu/kb/knowrob.owl#m10\"/>");
        out.println("");
        out.println("");
        out.println("<!-- http://ias.cs.tum.edu/kb/knowrob.owl#m11 -->");
        out.println("");
        out.println("<owl:DatatypeProperty rdf:about=\"http://ias.cs.tum.edu/kb/knowrob.owl#m11\"/>");
        out.println("");
        out.println("");
        out.println("<!-- http://ias.cs.tum.edu/kb/knowrob.owl#m12 -->");
        out.println("");
        out.println("<owl:DatatypeProperty rdf:about=\"http://ias.cs.tum.edu/kb/knowrob.owl#m12\"/>");
        out.println("");
        out.println("");
        out.println("<!-- http://ias.cs.tum.edu/kb/knowrob.owl#m13 -->");
        out.println("");
        out.println("<owl:DatatypeProperty rdf:about=\"http://ias.cs.tum.edu/kb/knowrob.owl#m13\"/>");
        out.println("");
        out.println("");
        out.println("<!-- http://ias.cs.tum.edu/kb/knowrob.owl#m20 -->");
        out.println("");
        out.println("<owl:DatatypeProperty rdf:about=\"http://ias.cs.tum.edu/kb/knowrob.owl#m20\"/>");
        out.println("");
        out.println("");
        out.println("<!-- http://ias.cs.tum.edu/kb/knowrob.owl#m21 -->");
        out.println("");
        out.println("<owl:DatatypeProperty rdf:about=\"http://ias.cs.tum.edu/kb/knowrob.owl#m21\"/>");
        out.println("");
        out.println("");
        out.println("<!-- http://ias.cs.tum.edu/kb/knowrob.owl#m22 -->");
        out.println("");
        out.println("<owl:DatatypeProperty rdf:about=\"http://ias.cs.tum.edu/kb/knowrob.owl#m22\"/>");
        out.println("");
        out.println("");
        out.println("<!-- http://ias.cs.tum.edu/kb/knowrob.owl#m23 -->");
        out.println("");
        out.println("<owl:DatatypeProperty rdf:about=\"http://ias.cs.tum.edu/kb/knowrob.owl#m23\"/>");
        out.println("");
        out.println("");
        out.println("<!-- http://ias.cs.tum.edu/kb/knowrob.owl#m30 -->");
        out.println("");
        out.println("<owl:DatatypeProperty rdf:about=\"http://ias.cs.tum.edu/kb/knowrob.owl#m30\"/>");
        out.println("");
        out.println("");
        out.println("<!-- http://ias.cs.tum.edu/kb/knowrob.owl#m31 -->");
        out.println("");
        out.println("<owl:DatatypeProperty rdf:about=\"http://ias.cs.tum.edu/kb/knowrob.owl#m31\"/>");
        out.println("");
        out.println("");
        out.println("<!-- http://ias.cs.tum.edu/kb/knowrob.owl#m32 -->");
        out.println("");
        out.println("<owl:DatatypeProperty rdf:about=\"http://ias.cs.tum.edu/kb/knowrob.owl#m32\"/>");
        out.println("");
        out.println("");
        out.println("<!-- http://ias.cs.tum.edu/kb/knowrob.owl#m33 -->");
        out.println("");
        out.println("<owl:DatatypeProperty rdf:about=\"http://ias.cs.tum.edu/kb/knowrob.owl#m33\"/>");
        out.println("");
        out.println("");
        out.println("<!-- http://ias.cs.tum.edu/kb/knowrob.owl#widthOfObject -->");
        out.println("");
        out.println("<owl:DatatypeProperty rdf:about=\"http://ias.cs.tum.edu/kb/knowrob.owl#widthOfObject\"/>");
        out.println("");
        out.println("");
        out.println("");
        out.println("");
        out.println("<!--");
        out.println("///////////////////////////////////////////////////////////////////////////////////////");
        out.println("//");
        out.println("// Classes");
        out.println("//");
        out.println("///////////////////////////////////////////////////////////////////////////////////////");
        out.println(" -->");
        out.println("");
        out.println("");
        out.println("<!-- http://ias.cs.tum.edu/kb/knowrob.owl#Cupboard -->");
        out.println("");
        out.println("<owl:Class rdf:about=\"http://ias.cs.tum.edu/kb/knowrob.owl#Cupboard\"/>");
        out.println("");
        out.println("");
        out.println("<!-- http://ias.cs.tum.edu/kb/knowrob.owl#RotationMatrix3D -->");
        out.println("");
        out.println("<owl:Class rdf:about=\"http://ias.cs.tum.edu/kb/knowrob.owl#RotationMatrix3D\"/>");
        out.println("");
        out.println("");
        out.println("<!-- http://ias.cs.tum.edu/kb/knowrob.owl#SemanticEnvironmentMap -->");
        out.println("");
        out.println("<owl:Class rdf:about=\"http://ias.cs.tum.edu/kb/knowrob.owl#SemanticEnvironmentMap\"/>");
        out.println("");
        out.println("");
        out.println("<!-- http://ias.cs.tum.edu/kb/knowrob.owl#SemanticMapPerception -->");
        out.println("");
        out.println("<owl:Class rdf:about=\"http://ias.cs.tum.edu/kb/knowrob.owl#SemanticMapPerception\"/>");
        out.println("");
        out.println("");
        out.println("<!-- http://ias.cs.tum.edu/kb/knowrob.owl#TimePoint -->");
        out.println("");
        out.println("<owl:Class rdf:about=\"http://ias.cs.tum.edu/kb/knowrob.owl#TimePoint\"/>");
        out.println("");
        out.println("");
        out.println("");
        out.println("");
        out.println("<!--");
        out.println("///////////////////////////////////////////////////////////////////////////////////////");
        out.println("//");
        out.println("// Individuals");
        out.println("//");
        out.println("///////////////////////////////////////////////////////////////////////////////////////");
        out.println("-->");
        out.println("");
        out.println("");
        out.println("<!-- http://ias.cs.tum.edu/kb/ias_semantic_map.owl#SemanticEnvironmentMap0 -->");
        out.println("");
        out.println("<owl:NamedIndividual rdf:about=\"http://ias.cs.tum.edu/kb/ias_semantic_map.owl#SemanticEnvironmentMap0\">");
        out.indent();
        out.println("<rdf:type rdf:resource=\"http://ias.cs.tum.edu/kb/knowrob.owl#SemanticEnvironmentMap\"/>");
        out.unindent();
        out.println("</owl:NamedIndividual>");
        out.println("");
        out.println("");
        out.println("");
        out.println("<!-- http://ias.cs.tum.edu/kb/ias_semantic_map.owl#timepoint_1344842159 -->");
        out.println("");
        out.println("<owl:NamedIndividual rdf:about=\"http://ias.cs.tum.edu/kb/ias_semantic_map.owl#timepoint_1344842159\">");
        out.indent();
        out.println("<rdf:type rdf:resource=\"http://ias.cs.tum.edu/kb/knowrob.owl#TimePoint\"/>");
        out.unindent();
        out.println("</owl:NamedIndividual>");
        out.println("");
        out.println("");
        out.println("");
        out.println("");
        out.println("");

        for (SemanticMapObject smo : smoList)
            smo.generateCode(out);

        out.unindent();
        out.println("</rdf:RDF>");
    }

}
